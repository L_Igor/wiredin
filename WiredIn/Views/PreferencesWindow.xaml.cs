﻿using System;
using System.Reflection;
using System.Windows;
using Microsoft.Win32;

namespace WiredIn.Views
{
    /// <summary>
    /// Interaction logic for PreferencesWindow.xaml
    /// </summary>
    public partial class PreferencesWindow : Window
    {
        public PreferencesWindow()
        {
            InitializeComponent();
        }

        private static string _startupPath = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";
        private static RegistryKey _registryStartupKey = Registry.CurrentUser.OpenSubKey(_startupPath, true);
        private static string _registryStartupKeyName = "WiredIn";

        private string AppPath
        {
            get { return Assembly.GetExecutingAssembly().Location; }
        }

        private void Startup_Initialized(object sender, EventArgs e)
        {
            var value = _registryStartupKey?.GetValue(_registryStartupKeyName)?.ToString();

            Startup.IsChecked = value == AppPath;
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (Startup.IsChecked.HasValue)
            {
                if (Startup.IsChecked.Value)
                {
                    _registryStartupKey?.SetValue(_registryStartupKeyName, AppPath);
                }
                else
                {
                    _registryStartupKey?.DeleteValue(_registryStartupKeyName, false);
                }
            }
        }
    }
}
