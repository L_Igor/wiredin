﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WiredIn.Commands;
using WiredIn.ViewModels;

namespace WiredIn.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowViewModel Model { get; set; }

        public MainWindow()
        {
            InitModel();

            InitializeComponent();
        }

        private void InitModel()
        {
            Model = new MainWindowViewModel(
                    new DelegateCommand(ShowCreatingStatus),
                    new DelegateCommand(HideCreatingStatus),
                    new DelegateCommand(ShowPreferences)
                );
            Model.Statuses = new ObservableCollection<LedStatusViewModel>(GetLedStatuses());
        }

        private static IEnumerable<LedStatusViewModel> GetLedStatuses()
        {
            return new[]
            {
                new LedStatusViewModel { Color = (Color)ColorConverter.ConvertFromString("#FF2BA39D"), Title = "Available" },
                new LedStatusViewModel { Color = (Color)ColorConverter.ConvertFromString("#FFCBAC43"), Title = "Meeting" },
                new LedStatusViewModel { Color = (Color)ColorConverter.ConvertFromString("#FFAB2328"), Title = "Wired In" },
            };
        }

        //private void BtnShowCreatingStatusSection_OnClick(object sender, RoutedEventArgs e)
        //{
        //    if (ColorBarSection.Visibility == Visibility.Hidden ||
        //        ColorBarSection.Visibility == Visibility.Collapsed)
        //    {
        //        ColorBarSection.Visibility = Visibility.Visible;
        //        ColorBarSection.UpdateLayout();
        //        this.Height = this.ActualHeight + ColorBarSection.ActualHeight;
        //    }
        //}

        //private void BtnShowPreferences_OnClick(object sender, RoutedEventArgs e)
        //{
        //    var preferencesWindow = new PreferencesWindow();
        //    preferencesWindow.ShowDialog();
        //}

        //private void BtnHideCreatingStatusSection_OnClick(object sender, RoutedEventArgs e)
        //{
        //    if (ColorBarSection.Visibility == Visibility.Visible)
        //    {
        //        this.Height = this.ActualHeight - ColorBarSection.ActualHeight;
        //        ColorBarSection.Visibility = Visibility.Collapsed;
        //    }
        //}

        private void ShowCreatingStatus()
        {
            if (ColorBarSection.Visibility == Visibility.Hidden ||
                ColorBarSection.Visibility == Visibility.Collapsed)
            {
                ColorBarSection.Visibility = Visibility.Visible;
                ColorBarSection.UpdateLayout();
                this.Height = this.ActualHeight + ColorBarSection.ActualHeight;
            }
        }

        private void HideCreatingStatus()
        {
            if (ColorBarSection.Visibility == Visibility.Visible)
            {
                this.Height = this.ActualHeight - ColorBarSection.ActualHeight;
                ColorBarSection.Visibility = Visibility.Collapsed;
            }
        }

        private void ShowPreferences()
        {
            var preferencesWindow = new PreferencesWindow();
            preferencesWindow.ShowDialog();
        }




        //private void ShowCreatingStatus()
        //{
        //    ColorBarSection.Visibility = Visibility.Visible;
        //    ColorBarSection.UpdateLayout();
        //    this.Height = this.ActualHeight + ColorBarSection.ActualHeight;
        //}

        //private bool CanShowCreatingStatus()
        //{
        //    return ColorBarSection.Visibility == Visibility.Hidden ||
        //           ColorBarSection.Visibility == Visibility.Collapsed;
        //}

        //private void HideCreatingStatus()
        //{
        //    this.Height = this.ActualHeight - ColorBarSection.ActualHeight;
        //    ColorBarSection.Visibility = Visibility.Collapsed;
        //}

        //private bool CanHideCreatingStatus()
        //{
        //    return ColorBarSection.Visibility == Visibility.Visible;
        //}
    }
}
