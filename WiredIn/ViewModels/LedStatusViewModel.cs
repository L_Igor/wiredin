﻿using System.Windows.Media;

namespace WiredIn.ViewModels
{
    public class LedStatusViewModel : BaseViewModel
    {
        private string _title;
        private Color _color;

        public string Title
        {
            get { return _title; }
            set
            {
                if (_title == value)
                    return;
                _title = value;
                OnPropertyChanged();
            }
        }

        public Color Color
        {
            get { return _color; }
            set
            {
                _color = value;
                OnPropertyChanged();
            }
        }
    }
}
