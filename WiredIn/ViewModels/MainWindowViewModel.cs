﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using WiredIn.Commands;

namespace WiredIn.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        public MainWindowViewModel(ICommand showCreatingStatusCommand, ICommand hideCreatingStatusCommand, ICommand showPreferencesCommand)
        {
            AddStatusCommand = new DelegateCommand<LedStatusViewModel>(AddStatus, CanAddStatus);
            ShowCreatingStatusCommand = showCreatingStatusCommand;
            HideCreatingStatusCommand = hideCreatingStatusCommand;
            ShowPreferencesCommand = showPreferencesCommand;

            NewStatus = new LedStatusViewModel();
        }

        public ICommand AddStatusCommand { get; }

        public ICommand ShowCreatingStatusCommand { get; }

        public ICommand HideCreatingStatusCommand { get; }

        public ICommand ShowPreferencesCommand { get; }

        private LedStatusViewModel _selectedStatus;

        public LedStatusViewModel SelectedStatus
        {
            get { return _selectedStatus; }
            set
            {
                if (_selectedStatus == value)
                    return;
                _selectedStatus = value;
                OnPropertyChanged();
            }
        }

        private LedStatusViewModel _newStatus;

        public LedStatusViewModel NewStatus
        {
            get { return _newStatus; }
            set
            {
                if (_newStatus == value)
                    return;

                if (_newStatus != null)
                    _newStatus.PropertyChanged -= OnNewStatusOnPropertyChanged;

                _newStatus = value;

                _newStatus.PropertyChanged += OnNewStatusOnPropertyChanged;

                OnPropertyChanged();
                OnNewStatusOnPropertyChanged(this, new PropertyChangedEventArgs(nameof(NewStatus)));
            }
        }

        private void OnNewStatusOnPropertyChanged(object s, PropertyChangedEventArgs e)
        {
            ((DelegateCommand<LedStatusViewModel>)AddStatusCommand).RaiseCanExecuteChanged();
        }

        public ObservableCollection<LedStatusViewModel> Statuses { get; set; } = new ObservableCollection<LedStatusViewModel>();

        private void AddStatus(LedStatusViewModel model)
        {
            Statuses.Add(model);
            NewStatus = new LedStatusViewModel()
            {
                Color = model.Color,
            };
        }

        private bool CanAddStatus(LedStatusViewModel model)
        {
            return model != null && !String.IsNullOrEmpty(model.Title);
        }
    }
}
